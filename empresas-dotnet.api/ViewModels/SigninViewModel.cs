﻿namespace empresas_dotnet.api.ViewModels
{
    public class SigninViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
