﻿namespace empresas_dotnet.api.ViewModels
{
    public class SigninResultViewModel
    {
        public string Access_token { get; set; }
        public string Investor_name { get; set; }
        public string Email { get; set; }
        public string City { get;  set; }
        public string Contry { get; set; }
        public decimal Balance { get; set; }
        public string Photo { get; set; }
        public string Portifolio { get; set; }
        public decimal Portifolio_value { get; set; }
        public bool First_access { get; set; }
        public bool Super_angel { get; set; }
    }
}
