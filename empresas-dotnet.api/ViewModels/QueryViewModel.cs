﻿using empresas_dotnet.api.Models.Enums;

namespace empresas_dotnet.api.ViewModels
{
    public class QueryViewModel
    {
        public EEnterpriseType enterprise_types { get; set; }
        public string name { get; set; }
    }
}
