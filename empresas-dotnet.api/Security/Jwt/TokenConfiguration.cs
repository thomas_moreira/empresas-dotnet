﻿namespace empresas_dotnet.api.Security.Jwt
{
    public class TokenConfiguration
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public string JwtKey { get; set; }
        public int Days { get; set; }
    }
}
