﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

namespace empresas_dotnet.api.Security.Jwt
{
    public class JwtTokenBuilder
    {
        private SecurityKey securityKey = null;
        private string subject = "";
        private string issuer = "";
        private string audience = "";
        private string nameId = "";        
        private List<Claim> claims = new List<Claim>();
        private int expiryInDays = 365;

        public JwtTokenBuilder AddSecurityKey(SecurityKey securityKey)
        {
            this.securityKey = securityKey;
            return this;
        }

        public JwtTokenBuilder AddSubject(string subject)
        {
            this.subject = subject;
            return this;
        }

        public JwtTokenBuilder AddIssuer(string issuer)
        {
            this.issuer = issuer;
            return this;
        }

        public JwtTokenBuilder AddAudience(string audience)
        {
            this.audience = audience;
            return this;
        }

        public JwtTokenBuilder AddNameId(string nameId)
        {
            this.nameId = nameId;
            return this;
        }

        public JwtTokenBuilder AddClaim(string type, string value)
        {
            var claim = new Claim(type, value);
            this.claims.Add(claim);
            return this;
        }

        public JwtTokenBuilder AddClaims(List<Claim> claims)
        {
            this.claims = claims;
            return this;
        }       

        public JwtTokenBuilder AddExpiryDays(int expiryInDays)
        {
            this.expiryInDays = expiryInDays;
            return this;
        }

        public string Build()
        {
            var claims = new List<Claim>
            {
              new Claim(JwtRegisteredClaimNames.Sub, this.subject),
              new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
              new Claim("NameId", this.nameId)
            }
            .Union(this.claims.Select(item => new Claim(item.Type, item.Value)));

            var token = new JwtSecurityToken(
                              issuer: this.issuer,
                              audience: this.audience,
                              claims: claims,
                              expires: DateTime.Now.AddDays(expiryInDays),                              
                              signingCredentials: new SigningCredentials(
                                                        this.securityKey,
                                                        SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
