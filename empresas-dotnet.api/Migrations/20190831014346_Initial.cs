﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace empresas_dotnet.api.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Enterprise",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Email_enterprise = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Facebook = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: true),
                    Twitter = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: true),
                    Linkedin = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: true),
                    Phone = table.Column<string>(type: "varchar(11)", maxLength: 11, nullable: true),
                    Own_enterprise = table.Column<bool>(nullable: false, defaultValue: false),
                    Enterprise_name = table.Column<string>(type: "varchar(1024)", maxLength: 1024, nullable: false),
                    Photo = table.Column<string>(type: "varchar(1024)", maxLength: 1024, nullable: true),
                    Description = table.Column<string>(type: "varchar(1024)", maxLength: 1024, nullable: true),
                    City = table.Column<string>(type: "varchar(80)", maxLength: 80, nullable: false),
                    Contry = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    Value = table.Column<int>(type: "integer", nullable: false),
                    Share_price = table.Column<decimal>(type: "money", nullable: false),
                    Enterprise_type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enterprise", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Email = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    Password = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Investor",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Investor_name = table.Column<string>(type: "varchar(256)", maxLength: 256, nullable: false),
                    Email = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false),
                    City = table.Column<string>(type: "varchar(80)", maxLength: 80, nullable: false),
                    Contry = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: false),
                    Balance = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    Photo = table.Column<string>(type: "varchar(1024)", maxLength: 1024, nullable: true),
                    Portifolio = table.Column<string>(type: "varchar(1024)", maxLength: 120, nullable: true),
                    Portifolio_value = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    First_access = table.Column<bool>(nullable: false, defaultValue: true),
                    Super_angel = table.Column<bool>(nullable: false, defaultValue: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Investor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Investor_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Enterprise",
                columns: new[] { "Id", "City", "Contry", "Description", "Email_enterprise", "Enterprise_name", "Enterprise_type", "Facebook", "Linkedin", "Phone", "Photo", "Share_price", "Twitter", "Value" },
                values: new object[] { new Guid("a2148a5a-75d8-4e12-b196-d0be880f3fce"), "Santiago", "Chile", null, "", "AllRide", 21, null, null, null, null, 0m, null, 0 });

            migrationBuilder.InsertData(
                table: "Enterprise",
                columns: new[] { "Id", "City", "Contry", "Description", "Email_enterprise", "Enterprise_name", "Enterprise_type", "Facebook", "Linkedin", "Phone", "Photo", "Share_price", "Twitter", "Value" },
                values: new object[] { new Guid("2f69287e-054d-43c8-a560-1e473edd3965"), "Viña del Mar", "Chile", null, "", "Alpaca Samka SpA", 7, null, null, null, null, 0m, null, 0 });

            migrationBuilder.InsertData(
                table: "Enterprise",
                columns: new[] { "Id", "City", "Contry", "Description", "Email_enterprise", "Enterprise_name", "Enterprise_type", "Facebook", "Linkedin", "Phone", "Photo", "Share_price", "Twitter", "Value" },
                values: new object[] { new Guid("803ce0c1-94ba-4e0f-bd68-282720a5c9ba"), "Maule", "Chile", null, "", "AQM S.A.", 1, null, null, null, null, 0m, null, 0 });

            migrationBuilder.InsertData(
                table: "Enterprise",
                columns: new[] { "Id", "City", "Contry", "Description", "Email_enterprise", "Enterprise_name", "Enterprise_type", "Facebook", "Linkedin", "Phone", "Photo", "Share_price", "Twitter", "Value" },
                values: new object[] { new Guid("eff77ebe-98d4-402f-8fd3-68ef727fa3ea"), "Santiago", "Chile", null, "", "Capta Hydro", 4, null, null, null, null, 0m, null, 0 });

            migrationBuilder.InsertData(
                table: "Enterprise",
                columns: new[] { "Id", "City", "Contry", "Description", "Email_enterprise", "Enterprise_name", "Enterprise_type", "Facebook", "Linkedin", "Phone", "Photo", "Share_price", "Twitter", "Value" },
                values: new object[] { new Guid("327d1759-f6ed-4f16-a31e-4a1ff3754f61"), "Santiago", "Chile", null, "", "urbanatika", 1, null, null, null, null, 0m, null, 0 });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "Email", "Password" },
                values: new object[] { new Guid("c5765c6b-8373-47b6-9442-33b3e60907e6"), "testeapple@ioasys.com.br", "Jf3pcIn3sYOr+gGm5mEknQ==" });

            migrationBuilder.InsertData(
                table: "Investor",
                columns: new[] { "Id", "Balance", "City", "Contry", "Email", "Investor_name", "Photo", "Portifolio", "Portifolio_value", "UserId" },
                values: new object[] { new Guid("811487a0-6fba-4cd5-b28a-ae0dcc0c7f16"), 10m, "BH", "Brasil", "testeapple@ioasys.com.br", "Teste Apple", null, null, 10m, new Guid("c5765c6b-8373-47b6-9442-33b3e60907e6") });

            migrationBuilder.CreateIndex(
                name: "IX_Investor_UserId",
                table: "Investor",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Enterprise");

            migrationBuilder.DropTable(
                name: "Investor");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
