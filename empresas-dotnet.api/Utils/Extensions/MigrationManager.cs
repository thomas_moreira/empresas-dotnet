﻿using empresas_dotnet.api.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace empresas_dotnet.api.Utils.Extensions
{
    public static class MigrationManager
    {
        public static IWebHost MigrateDatabase(this IWebHost webHost)
        {
            using (var scope = webHost.Services.CreateScope())
            {
                using (var appContext = scope.ServiceProvider.GetRequiredService<AppDataContext>())
                {
                    try
                    {
                        appContext.Database.EnsureDeleted();                     
                        appContext.Database.Migrate();
                    }
                    catch (Exception)
                    {                        
                        throw;
                    }
                }
            }

            return webHost;
        }
    }
}
