﻿using empresas_dotnet.api.Shared;

namespace empresas_dotnet.api.Models
{
    public class User : Entity
    {
        public User(string email, string password)
        {
            Email = email;
            Password = password;
        }

        public string Email { get; private set; }
        public string Password { get; private set; }
    }
}
