﻿namespace empresas_dotnet.api.Models.Enums
{
    public enum EEnterpriseType
    {
        
        Agro = 1,
        Biotech = 3,
        Eco = 4,
        Ecommerce = 5,
        Education = 6,
        Fashion = 7,
        Fintech = 8,
        Software = 21

    }
}
