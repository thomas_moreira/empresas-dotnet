﻿using empresas_dotnet.api.Shared;
using System;

namespace empresas_dotnet.api.Models
{
    public class Investor : Entity
    {
        protected Investor() { }

        public Investor(string investor_name, 
                        string email, 
                        string city, 
                        string contry, 
                        decimal balance, 
                        string photo, 
                        string portifolio, 
                        decimal portifolio_value, 
                        bool first_access, 
                        bool super_angel,
                        Guid userId)
        {
            Investor_name = investor_name;
            Email = email;
            City = city;
            Contry = contry;
            Balance = balance;
            Photo = photo;
            Portifolio = portifolio;
            Portifolio_value = portifolio_value;
            First_access = first_access;
            Super_angel = super_angel;
            UserId = userId;
        }

        public string Investor_name { get; private set; }
        public string Email { get; private set; }
        public string City { get; private set; }
        public string Contry { get; private set; }
        public decimal Balance { get; private set; }
        public string Photo { get; private set; }
        public string Portifolio { get; private set; }
        public decimal Portifolio_value { get; private set; }
        public bool First_access { get; private set; }
        public bool Super_angel { get; private set; }
        public Guid UserId { get; private set; }
        public User User { get; private set; }
    }
}
