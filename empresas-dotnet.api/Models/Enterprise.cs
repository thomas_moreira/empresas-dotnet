﻿using empresas_dotnet.api.Models.Enums;
using empresas_dotnet.api.Shared;

namespace empresas_dotnet.api.Models
{
    public class Enterprise : Entity
    {
        protected Enterprise() { }

        public Enterprise(string email_enterprise, 
                          string enterprise_name, 
                          string city, 
                          string contry, 
                          EEnterpriseType enterprise_type)
        {
            Email_enterprise = email_enterprise;            
            Enterprise_name = enterprise_name;            
            City = city;
            Contry = contry;                        
            Enterprise_type = enterprise_type;
        }

        public string Email_enterprise { get; private set; }
        public string Facebook { get; private set; }
        public string Twitter { get; private set; }
        public string Linkedin { get; private set; }
        public string Phone { get; private set; }
        public bool Own_enterprise { get; private set; }
        public string Enterprise_name { get; private set; }
        public string Photo { get; private set; }
        public string Description { get; private set; }
        public string City { get; private set; }
        public string Contry { get; private set; }
        public int Value { get; private set; }
        public decimal Share_price { get; private set; }
        public EEnterpriseType Enterprise_type { get; private set; }


        public void AddFacebook(string facebook)
        {
            Facebook = facebook;
        }
        public void AddTwitter(string twitter)
        {
            Twitter = twitter;
        }
        public void AddLinkedin(string linkedin)
        {
            Linkedin = linkedin;
        }
        public void AddPhone(string phone)
        {
            Phone = phone;
        }
        public void AddPhoto(string photo)
        {
            Photo = photo;
        }
        public void AddDescription(string description)
        {
            Description = description;
        }
        public void SetValue(int value)
        {
            Value = value;
        }
        public void SetSharePrice(decimal sharePrice)
        {
            Share_price = sharePrice;
        }



    }
}
