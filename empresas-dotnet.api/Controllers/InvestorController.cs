﻿using empresas_dotnet.api.Data.Respositories;
using empresas_dotnet.api.Security.Jwt;
using empresas_dotnet.api.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace empresas_dotnet.api.Controllers
{
    public class InvestorController : Controller
    {
        private readonly InvestorRepository _investorRepository;


        public InvestorController(InvestorRepository userRepository)
        {
            _investorRepository = userRepository;
        }

        [Route("api/v1/users/auth/sign_in")]
        [HttpPost]
        [AllowAnonymous]
        public IActionResult Signin([FromServices]TokenConfiguration tokenConfigurations,
                                    [FromBody] SigninViewModel model)
        {
            string token = string.Empty;

            //Recupera Investor validando o User
            var investor = _investorRepository.GetInvestorByUser(model.Email, model.Password);

            //Verifica se existe o Investor/User requisitado
            if (investor != null && investor.User != null)
            {
                //gera o token de acesso
                token = new JwtTokenBuilder()
                       .AddSecurityKey(JwtSecurityKey.Create(tokenConfigurations.JwtKey))
                       .AddSubject(investor.User.Email)
                       .AddIssuer(tokenConfigurations.Issuer)
                       .AddAudience(tokenConfigurations.Audience)
                       .AddNameId(investor.Id.ToString())
                       .AddExpiryDays(tokenConfigurations.Days)
                       .Build();


                return Ok(new SigninResultViewModel
                {
                    Access_token = token,
                    Investor_name = investor.Investor_name,
                    Email = investor.Email,
                    City = investor.City,
                    Contry = investor.Contry,
                    Balance = investor.Balance,
                    Photo = investor.Photo,
                    Portifolio = investor.Portifolio,
                    Portifolio_value = investor.Portifolio_value,
                    First_access = investor.First_access,
                    Super_angel = investor.Super_angel
                });
            }
            else
            {
                //retornar erro de validação para o usuario
                return BadRequest(new { message = "Usuário ou senha inválidos"});
            }

        }
    }
}