﻿using empresas_dotnet.api.Data.Respositories;
using empresas_dotnet.api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;

namespace empresas_dotnet.api.Controllers
{
    public class EnterpriseController : Controller
    {
        private readonly EnterpriseRepository _enterpriseRepository;

        public EnterpriseController(EnterpriseRepository enterpriseRepository)
        {
            _enterpriseRepository = enterpriseRepository;
        }

        [Route("api/v1/enterprises")]
        [HttpGet]
        public ResultViewModel Get()
        {
            try
            {
                var list = _enterpriseRepository.Get();
                return new ResultViewModel { Success = true, Message = null, Data = list };

            }
            catch (Exception)
            {
                return new ResultViewModel
                {
                    Success = false,
                    Message = "Não foi posível processar sua requisição. Tente novamente mais tarde.",
                    Data = null
                };
            }
        }

        [Route("api/v1/enterprises/{id:guid}")]
        [HttpGet]
        public ResultViewModel Get(Guid id)
        {
            try
            {
                var enterprise = _enterpriseRepository.GetById(id);
                return new ResultViewModel { Success = true, Message = null, Data = enterprise };

            }
            catch (Exception)
            {
                return new ResultViewModel
                {
                    Success = false,
                    Message = "Não foi posível processar sua requisição. Tente novamente mais tarde.",
                    Data = null
                };
            }
        }

        [Route("api/v1/enterprises/query")]
        [HttpGet]
        public ResultViewModel Find([FromQuery] QueryViewModel query)
        {
            try
            {
                var list = _enterpriseRepository.Find(query);
                return new ResultViewModel { Success = true, Message = null, Data = list };

            }
            catch (Exception)
            {
                return new ResultViewModel
                {
                    Success = false,
                    Message = "Não foi posível processar sua requisição. Tente novamente mais tarde.",
                    Data = null
                };
            }

        }


    }
}