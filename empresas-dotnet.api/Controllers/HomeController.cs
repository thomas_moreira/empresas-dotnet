﻿using Microsoft.AspNetCore.Mvc;

namespace empresas_dotnet.api.Controllers
{
    public class HomeController : Controller
    {
        [Route("api/v1/")]
        [HttpGet]
        public IActionResult Index()
        {
            return Ok("Api is running");
        }
    }
}