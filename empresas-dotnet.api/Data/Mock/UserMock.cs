﻿using empresas_dotnet.api.Models;
using empresas_dotnet.api.Utils.Security;

namespace empresas_dotnet.api.Data.Mock
{
    public static class UserMock
    {
        public static User GetuserMock()
        {
            return new User("testeapple@ioasys.com.br", Crypt.Encrypt("12341234"));
        }
    }
}
