﻿using empresas_dotnet.api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.api.Data.Maps
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        private User _user;
        public UserMap(User user)
        {
            _user = user;
        }

        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");
            builder.HasKey(x => x.Id);            
            builder.Property(x => x.Email).IsRequired().HasMaxLength(100).HasColumnType("varchar(100)");
            builder.Property(x => x.Password).IsRequired().HasMaxLength(256).HasColumnType("varchar(256)");

            builder.HasData(_user);

        }
    }
}
