﻿using empresas_dotnet.api.Models;
using empresas_dotnet.api.Models.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.api.Data.Maps
{
    public class EnterpriseMap : IEntityTypeConfiguration<Enterprise>
    {
        public void Configure(EntityTypeBuilder<Enterprise> builder)
        {
            builder.ToTable("Enterprise");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Email_enterprise).IsRequired().HasMaxLength(100).HasColumnType("varchar(100)");
            builder.Property(x => x.Facebook).HasMaxLength(256).HasColumnType("varchar(256)");
            builder.Property(x => x.Twitter).HasMaxLength(256).HasColumnType("varchar(256)");
            builder.Property(x => x.Linkedin).HasMaxLength(256).HasColumnType("varchar(256)");
            builder.Property(x => x.Phone).HasMaxLength(11).HasColumnType("varchar(11)");
            builder.Property(x => x.Own_enterprise).IsRequired().HasDefaultValue(false);
            builder.Property(x => x.Enterprise_name).IsRequired().HasMaxLength(1024).HasColumnType("varchar(1024)");
            builder.Property(x => x.Photo).HasMaxLength(1024).HasColumnType("varchar(1024)");
            builder.Property(x => x.Description).HasMaxLength(1024).HasColumnType("varchar(1024)");
            builder.Property(x => x.City).IsRequired().HasMaxLength(80).HasColumnType("varchar(80)");
            builder.Property(x => x.Contry).IsRequired().HasMaxLength(50).HasColumnType("varchar(50)");
            builder.Property(x => x.Value).HasColumnType("integer");
            builder.Property(x => x.Share_price).HasColumnType("money");
            builder.Property(x => x.Enterprise_type).IsRequired();

            builder.HasData(
                new Enterprise(string.Empty, "AllRide", "Santiago", "Chile", EEnterpriseType.Software),
                new Enterprise(string.Empty, "Alpaca Samka SpA", "Viña del Mar", "Chile", EEnterpriseType.Fashion),
                new Enterprise(string.Empty, "AQM S.A.", "Maule", "Chile", EEnterpriseType.Agro),
                new Enterprise(string.Empty, "Capta Hydro", "Santiago", "Chile", EEnterpriseType.Eco),
                new Enterprise(string.Empty, "urbanatika", "Santiago", "Chile", EEnterpriseType.Agro)
                );

        }
    }
}
