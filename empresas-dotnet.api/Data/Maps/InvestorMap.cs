﻿using empresas_dotnet.api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.api.Data.Maps
{
    public class InvestorMap : IEntityTypeConfiguration<Investor>
    {
        private readonly User _user;

        public InvestorMap(User user)
        {
            _user = user;
        }


        public void Configure(EntityTypeBuilder<Investor> builder)
        {
            builder.ToTable("Investor");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Investor_name).IsRequired().HasMaxLength(256).HasColumnType("varchar(256)");
            builder.Property(x => x.Email).IsRequired().HasMaxLength(100).HasColumnType("varchar(100)");
            builder.Property(x => x.City).IsRequired().HasMaxLength(80).HasColumnType("varchar(80)");
            builder.Property(x => x.Contry).IsRequired().HasMaxLength(50).HasColumnType("varchar(50)");
            builder.Property(x => x.Balance).IsRequired().HasColumnType("decimal(10,2)");
            builder.Property(x => x.Photo).HasMaxLength(1024).HasColumnType("varchar(1024)");
            builder.Property(x => x.Portifolio).HasMaxLength(120).HasColumnType("varchar(1024)");
            builder.Property(x => x.Portifolio_value).HasColumnType("decimal(10,2)");
            builder.Property(x => x.First_access).HasDefaultValue(true);
            builder.Property(x => x.Super_angel).HasDefaultValue(false);
            builder.HasOne(x => x.User);

            builder.HasData(new Investor("Teste Apple", "testeapple@ioasys.com.br", "BH", "Brasil", 10, null, null, 10, false, false, _user.Id));
        }
    }

}