﻿using empresas_dotnet.api.Data.Maps;
using empresas_dotnet.api.Data.Mock;
using empresas_dotnet.api.Models;
using Microsoft.EntityFrameworkCore;

namespace empresas_dotnet.api.Data
{
    public class AppDataContext : DbContext
    {        
        public DbSet<Investor> Investors { get; set; }
        public DbSet<Enterprise> Enterprises { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {            
            optionsBuilder.UseSqlServer("ConnectionString");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            var user = UserMock.GetuserMock();

            builder.ApplyConfiguration(new InvestorMap(user));
            builder.ApplyConfiguration(new UserMap(user));
            builder.ApplyConfiguration(new EnterpriseMap());
                       
            

        }
    }
}
