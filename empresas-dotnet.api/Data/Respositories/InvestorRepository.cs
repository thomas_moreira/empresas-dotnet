﻿using empresas_dotnet.api.Models;
using empresas_dotnet.api.Utils.Security;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace empresas_dotnet.api.Data.Respositories
{
    public class InvestorRepository
    {
        private readonly AppDataContext _context;

        public InvestorRepository(AppDataContext context)
        {
            _context = context;

        }

        public Investor GetInvestorByUser(string email, string password)
        {
            return _context.Investors
                .Include(x => x.User)
                .FirstOrDefault(x => x.User.Email == email && x.User.Password == Crypt.Encrypt(password));
        }
    }
}
