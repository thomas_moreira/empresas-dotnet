﻿using empresas_dotnet.api.Models;
using empresas_dotnet.api.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace empresas_dotnet.api.Data.Respositories
{
    public class EnterpriseRepository
    {
        private readonly AppDataContext _context;

        public EnterpriseRepository(AppDataContext context)
        {
            _context = context;
        }

        public IEnumerable<Enterprise> Get()
        {
            return _context.Enterprises
                .AsNoTracking()
                .ToList();
        }

        public Enterprise GetById(Guid id)
        {
            return _context.Enterprises
                .AsNoTracking()
                .Where(x => x.Id == id)
                .FirstOrDefault();
        }

        public IEnumerable<Enterprise> Find(QueryViewModel query)
        {
            return (from e in _context.Enterprises
                    where e.Enterprise_type == query.enterprise_types && 
                          EF.Functions.Like(e.Enterprise_name, '%' + query.name + '%')
                    select e).ToList();


        }
    }
}
