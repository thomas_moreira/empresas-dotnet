# README #

## ---- Português ---- ##

Este documento README tem como objetivo fornecer as informações necessárias para executar e testar o projeto Empresas.

### Informações Importantes ###


* Antes de executar o projeto, informar a ConnectionString, na classe Data\AppDataContext.

* Ao executar o projeto, o banco de dados será criado e populado automaticamente, com os dados para teste.

* O modelo de autenticação e autorização foi implementado utilizando JWT (Json Web Token).

* Neste repositório está disponibilizada uma collection para Postman, contendo os endpoints para teste.


### Dados para Teste ###

* Servidor: http://localhost:3001
* Versão da API: v1
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234


